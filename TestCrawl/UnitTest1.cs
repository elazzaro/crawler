﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CrawlerProto;
using System.Xml;

namespace TestCrawl
{
    [TestClass]
    public class UnitTest1
    {
        const string knownUri = "http://wiprodigital.com";
        const string knownInternalUri = @"https://wiprodigital.com/who-we-are/#masthead";
        const string knownExternalUri = @"https://www.facebook.com/WiproDigital/";

        [TestMethod]
        public void TestWillCrawlInternalLinks()
        {
            // arrange
            Crawler crawler = new Crawler();

            // act
            string result = crawler.Crawl(knownUri);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result); 
            
            // assert
            // find a known internal link, known to have children, and verify that it was followed
            var localLinks = doc.SelectSingleNode("//CrawlNode[strUri='" + knownInternalUri + "'][1]/localLinks");
            var externalLinks = doc.SelectSingleNode("//CrawlNode[strUri='" + knownInternalUri + "'][1]/externalLinks");
            Assert.IsTrue(localLinks.ChildNodes.Count + externalLinks.ChildNodes.Count > 0);
        }

        [TestMethod]
        public void TestWillNotCrawlExternalLinks()
        {
            // arrange
            Crawler crawler = new Crawler();

            // act
            string result = crawler.Crawl(knownUri);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result);

            // assert
            // find a known external link, known to have children, and verify that it was not followed
            var localLinks = doc.SelectSingleNode("//CrawlNode[strUri='" + knownExternalUri + "'][1]/localLinks");
            var externalLinks = doc.SelectSingleNode("//CrawlNode[strUri='" + knownExternalUri + "'][1]/externalLinks");
            Assert.IsTrue(localLinks.ChildNodes.Count + externalLinks.ChildNodes.Count == 0);
        }
    }
}
