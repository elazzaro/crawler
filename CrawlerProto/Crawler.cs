﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Xml.Serialization;

namespace CrawlerProto
{
    public class Crawler
    {
        public class CrawlNode
            // must be public for XmlSerializer
        {
            // storing strings instead of a Uri because XmlSerializer doesn't handle Uri
            public string strUri;
            public string strHost;
            public List<CrawlNode> localLinks;
            public List<CrawlNode> externalLinks;
            public List<CrawlNode> staticLinks;

            public CrawlNode(Uri current)
            {
                strUri = current.ToString();
                strHost = current.Host;
                localLinks = new List<CrawlNode>();
                externalLinks = new List<CrawlNode>();
                staticLinks = new List<CrawlNode>();
            }

            public CrawlNode() { }
        }

        Queue<CrawlNode> crawlQueue;

        List<string> visited;

        public Crawler()
        {
            crawlQueue = new Queue<CrawlNode>();
            visited = new List<string>();
        }


        public string Crawl(string startURL)
        {
            // seed the queue with the root URI
            CrawlNode root = new CrawlNode(new Uri(startURL));
            crawlQueue.Enqueue(root);

            // crawl the queue
            CrawlQueue();

            // convert output to string
            //return CrawlFormat(root, "");
            return CrawlXml(root);
        }


        void CrawlQueue()
        {
            // loop until queue is empty
            while (crawlQueue.Count > 0)
            {
                // dequeue one node and check visited list
                CrawlNode node = crawlQueue.Dequeue();
                int index = visited.BinarySearch(node.strUri);
                if (index < 0)
                {
                    // we haven't been there.  Insert in sorted location
                    visited.Insert(~index, node.strUri);

                    // load document & get list of links
                        // code to handle file:// format would go here
                    HtmlWeb hw = new HtmlWeb();
                    HtmlDocument doc;
                    try
                    {
                        doc = hw.Load(node.strUri);
                    }
                    catch
                    {
                        // for quick-and-dirty, skip bad URLs -- production code would have to handle somehow
                        return;
                    }

                    var links = doc.DocumentNode.SelectNodes("//a[@href]");
                    if (links != null)
                    {

                        foreach (HtmlNode link in links)
                        {
                            // classify & add to proper list
                            CrawlNode newNode = new CrawlNode(new Uri(new Uri(node.strUri), link.Attributes["href"].Value));
                            if (node.strHost == newNode.strHost)
                                node.localLinks.Add(newNode);
                            else
                                node.externalLinks.Add(newNode);
                        }

                        // add local links to queue
                        foreach (var childNode in node.localLinks)
                        {
                            crawlQueue.Enqueue(childNode);
                        }
                    }
                }
            }
        }
        

        string CrawlFormat(CrawlNode node, string indent)
        {
            // represent current node
            StringBuilder formatted = new StringBuilder(indent + node.strUri);
            formatted.AppendLine();

            string labelIndent = indent + "   ";
            string nextIndent = labelIndent + "   ";

            // "Local" and recurse local list (if any) with increased indent
            if (node.localLinks.Count > 0)
            {
                formatted.AppendLine(labelIndent + "Local:");
                foreach (var link in node.localLinks)
                    formatted.Append(CrawlFormat(link, nextIndent));
            }

            // "External" and recurse external list (if any) with increased indent
            if (node.externalLinks.Count > 0)
            {
                formatted.AppendLine(labelIndent + "External:");
                foreach (var link in node.externalLinks)
                    formatted.Append(CrawlFormat(link, nextIndent));
            }

            // "Static" and recurse static list (if any) with increased indent
            if (node.staticLinks.Count > 0)
            {
                formatted.AppendLine(labelIndent + "Static:");
                foreach (var link in node.staticLinks)
                    formatted.Append(CrawlFormat(link, nextIndent));
            }

            // return string
            return formatted.ToString();
        }

        string CrawlXml(CrawlNode node)
        {
            var stringwriter = new System.IO.StringWriter();
            var serializer = new XmlSerializer(typeof(CrawlNode));
            serializer.Serialize(stringwriter, node);
            return stringwriter.ToString();
        }
    }
}
