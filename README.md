CrawlProto minimal webcrawler
=============================

## How to build, test, and run

CrawlProto is a Visual Studio 2013 solution.  One external library is used,  HTML Agility Pack, which is available through NuGet.  You should be able to download the 
repository, add HTML Agility Pack, load into the IDE and then build and run the tests via the BUILD and TEST menus.

Two test harnesses are provided:  a very simple Windows program for ad hoc testing, and a minimal set of automated tests.

The Windows harness is set as the start project, so it can be run via the Start button or the Start options on the DEBUG menu.  It presents a textbox where the tester can 
enter a root url.  When the Crawl button is pressed, it calls the main Crawl method, presents a file dialog, saves the output to the selected file, and opens it in Wordpad.

The automated tests use the Microsoft framework and can be run from the TEST menu.


## Trade-offs and limitations

Because I had not worked with HTML parsers before, I started with a prototype and an ad hoc test harness in order to get some familiarity with the libraries before
writing tests.  I intended to start over again and write a completely new final version using TDD methodology.  Unfortunately, due to unavoidable personal 
cirumstances, several calendar days passed with no opportunity to do meaningful work on the project.  Rather than delay further, I am presenting the prototype,
with automated tests added after the fact.

Also note that these are not unit tests -- they require a live internet connection.  HTML Agility Pack does not handle file:// URIs in the same way that it handles http:// 
URIs, so further code changes would be required to implement isolated unit tests.  I'll discuss this more under Further Developments.

XmlSerializer would not work unless all fields in CrawlNode were public.  This would not be acceptable in production code.  There is an alternate reporting method
(CrawlFormat()) that worked with

Finally, this code does not properly handle static links (such as images) as specified.  I have not found a reliable way to identify them in the HTML.


## Possible further development

Given more time, I would like to deal with:

* Isolated unit tests.  I see two possible approaches:
    - Use file:// URIs to store test input in local files.  CrawlQueue() would have to be modified to load the HtmlDocument differently based on the URI type, and you would
	need to include the existing automated tests in order to exercise all code paths, but you would have unit tests for most of it.
	- Mock HttpMessageHandler to get complete control of visited content.  For this approach, you would use HttpClient.GetStringAsync() to get the content and load the 
	HtmlDocument from the string, instead of using HtmlWeb.  This would have taken considerably more time to implement.

* XML serialization without making all fields public -- possibly with DataContractSerializer.

* Identify and report static resource links properly (obviously).

* Performance: One of the automated tests takes much longer than the other.  I would like to figure out why and see if I could get the runtime down.

